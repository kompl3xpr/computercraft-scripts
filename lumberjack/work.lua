FUEL_SLOT = 1
SAPLING_SLOT = 2
MIN_FUEL_LEVEL = 500
LOG_NAME = "minecraft:spruce_log"

turtle.turnBack = function()
    turtle.turnLeft()
    turtle.turnLeft()
end

turtle.prepare = function()
    turtle.should_plant = false
    turtle.handleFuel()

    -- Suck saplings
    turtle.forward()
    turtle.turnLeft()
    turtle.forward()
    turtle.turnRight()

    turtle.select(SAPLING_SLOT)
    turtle.suckDown()
    if (turtle.getItemCount()) == 0 then
        print(string.format("[WARN %s] Lack of saplings.", os.date("%m-%d %X")))
    end

    turtle.turnRight()
    turtle.forward()
    turtle.turnLeft()
end

turtle.handleFuel = function()
    if (turtle.getFuelLevel() < MIN_FUEL_LEVEL) then
        turtle.select(FUEL_SLOT)
        turtle.suckDown()
        print(string.format("[INFO %s] Refueling...", os.date("%m-%d %X")))
        turtle.refuel()
    end
    print(string.format("[INFO %s] Fuel Level: %d.", os.date("%m-%d %X"), turtle.getFuelLevel()))
end

turtle.plantBack = function()
    turtle.turnBack()
    turtle.place()
    turtle.turnBack()
end

turtle.forwardAndWork = function(times)
    times = times or 1
    for _ = 1, times do
        if (turtle.tryChop()) then
            turtle.should_plant = true
        else
            turtle.forward()
            if (turtle.should_plant) then
                turtle.plantBack()
                turtle.should_plant = false
            end
            if (turtle.detectDown()) then
                turtle.should_plant = true
            end
        end
    end
end

turtle.tryChop = function()
    if (not turtle.detect()) then
        return false
    end
    turtle.dig()
    turtle.forward()

    local n = 0
    while true do
        local has_block, block = turtle.inspectUp()
        if not has_block or block.name ~= LOG_NAME then
            break
        end
        turtle.digUp()
        turtle.up()
        n = n + 1
    end

    for _ = 1, n do
        turtle.down()
    end
    return true
end


local function work()
    turtle.prepare()
    for _ = 1, 5 do
        turtle.forward()
    end

    local steps = {
        { "turnLeft", 6 }, { "turnRight", 8 }, { "turnRight", 4 },
        { "turnRight", 4 }, { "turnLeft", 4 }, { "turnLeft", 4 },
        { "turnRight", 4 }, { "turnRight", 8 }, { "turnRight", 6 }
    }
    for _, m in ipairs(steps) do
        turtle[m[1]]()
        turtle.forwardAndWork(m[2])
    end

    for slot = 1, 16 do
        turtle.select(slot)
        turtle.dropDown()
    end

    -- go back
    turtle.turnLeft()
    for _ = 1, 6 do
        turtle.forward()
    end
    turtle.turnBack()
end

while true do
    print(string.format("[INFO %s] Start to work.", os.date("%m-%d %X")))
    work()
    print(string.format("[INFO %s] Sleeping for 20 minutes...", os.date("%m-%d %X")))
    sleep(1200)
end
