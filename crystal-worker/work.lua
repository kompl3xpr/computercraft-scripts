OUT1 = "ae2:certus_quartz_crystal"
OUT2 = "ae2:fluix_crystal"

turtle.suckLeft = function(n)
    turtle.turnLeft()
    turtle.suck(n)
    turtle.turnRight()
end

turtle.suckRight = function(n)
    turtle.turnRight()
    turtle.suck(n)
    turtle.turnLeft()
end

function Log(type, msg)
    print(string.format("[%s %s] %s", type, os.date("%m-%d %X"), msg))
end

while true do
    Log("INFO", "Crafting...")
    local err = false
    for _ = 1, 16 do
        turtle.select(1)
        turtle.suck(64 - turtle.getItemCount())
        if (turtle.getItemCount() < 64) then
            Log("ERROR", "Lack of Sands.")
            err = true
            break
        end

        for _, f in ipairs({ "suckLeft", "suckRight" }) do
            turtle.select(2)
            turtle[f](32)

            turtle.select(16)
            turtle.craft()
            turtle.dropDown()
        end
    end
    if err then break end

    turtle.select(12)

    Log("INFO", "Sleeping for 20 minutes...")
    sleep(1210)

    Log("INFO", "Collecting...")
    for _ = 1, 16 do
        turtle.suckDown()

        local item = turtle.getItemDetail()
        if (not item) then break end
        if (item.name == OUT1 or item.name == OUT2) then
            turtle.dropUp()
        else
            Log("WARN", string.format("Invalid Output Items(%s)!", item.name))
            turtle.dropDown()
        end
    end
end
