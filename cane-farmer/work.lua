-- 初始配置:
--   1. 你的有效甘蔗场是一个矩形
--   2. 将全局变量 `WIDTH` 和 `LENGTH` 设置为甘蔗场的边（默认是一个区块的大小）
--   3. 按下图摆好你的机器
--   4. 执行该程序

-- 俯视图:
--   |------WIDTH------|   ___
--   x x x x x x x x x x    |
--   x x x x x x x x x x  LENGTH
--   x x x x x x x x x x    |       其中
--   x x x x x x x x x x   _|_        `x`: 甘蔗
--   ^                 .              `^`: 海龟（朝向即箭头方向，在图中是向上）
--                                    `.`: 若 `WIDTH` 为偶数，此处应该是空气

-- 正视图:
--   [@][$]        其中
--   [_]             `[@]`: 海龟的背部
--   ######          `[_]`: 海龟输出甘蔗的容器
--                   `[$]`: 燃料存储
--                   `###`: 地面（和甘蔗场的地面齐平）

-- 在这里设置甘蔗场的规模:
LENGTH = 16
WIDTH = 16



FUEL_LEVEL_MIN = LENGTH * WIDTH * 3
GROWTH_DURATION = 10 * 60 * 3
DEST_X = ({ WIDTH % 2 == 0 } and { 1 } or { LENGTH })[1]

local forwardAndCollect = function()
    turtle.dig()
    turtle.forward()
    turtle.suckDown()
end

turtle.turn = function(toRight)
    if toRight then
        return turtle.turnRight()
    end
    return turtle.turnLeft()
end

turtle.traverseFrom = function(forward, toRight, x, y)
    if x == DEST_X and y == WIDTH then
        return forward()
    end
    if x == LENGTH and y % 2 == 1 then
        turtle.turn(toRight)
        forward()
        turtle.turn(toRight)
        return turtle.traverseFrom(forward, toRight, x, y + 1)
    end
    if x == 1 and y % 2 == 0 then
        turtle.turn(not toRight)
        forward()
        turtle.turn(not toRight)
        return turtle.traverseFrom(forward, toRight, x, y + 1)
    end
    forward()
    return turtle.traverseFrom(forward, toRight, x + y % 2 * 2 - 1, y)
end

turtle.collectAll = function()
    turtle.traverseFrom(forwardAndCollect, true, 0, 1)
end

turtle.goBack = function()
    turtle.turnLeft()
    turtle.turnLeft()
    turtle.traverseFrom(forwardAndCollect, false, 0, 1)
    turtle.turnLeft()
    turtle.turnLeft()
end

turtle.tryRefuel = function()
    if turtle.getFuelLevel() >= FUEL_LEVEL_MIN then
        return true
    end
    turtle.turnRight()
    turtle.suck()
    turtle.refuel()
    turtle.turnLeft()
    return turtle.getFuelLevel() >= FUEL_LEVEL_MIN
end

local function work()
    if not turtle.tryRefuel() then
        error("[ERROR] LACK OF FUEL!")
        return
    end
    turtle.collectAll()
    turtle.goBack()
    for i = 1, 16 do
        turtle.select(i)
        turtle.dropDown()
    end
    sleep(GROWTH_DURATION)
    return work()
end

work()
